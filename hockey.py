import sys
import os


USER_AGENT = ':http-user-agent="PS4 libhttp/1.60"'
bitrates = ['400', '800', '1200', '1600', '2400', '3000', '4500']

def parse_schedule():
    import urllib2
    from xml.dom.minidom import parseString
    urls = []

    file = urllib2.urlopen("http://breadwinka.com/get_games.php")
    data = file.read()
    file.close()
    # print data
    tag_name = 'ipad_url'

    dom = parseString(data)
    for i in xrange(len(dom.getElementsByTagName(tag_name))):
        xml_tag = dom.getElementsByTagName(tag_name)[i].toxml()
        url = xml_tag.replace('<'+tag_name+'>', '').replace('</'+tag_name+'>','')
        for j in xrange(len(bitrates)):
            urls.append(url.replace('ipad', bitrates[j]))

    return urls


def launch_jar(url):
    command = "sudo java -jar FuckNeulionV2.jar " + url + " &"
    os.system(command)
    return 0


def launch_vlc(url):
    command = "/Applications/VLC.app/Contents/MacOS/VLC -vvv " + url + " " + USER_AGENT + " & > vlc.txt"
    os.system(command)
    return 0


def main():
    urls = parse_schedule()
    print "Which NHL game do you want to watch?"
    for i in xrange(len(urls)):
        print i, '  ', urls[i]
    choice = raw_input("Enter the number associated with the game you want to watch \n")
    choice = int(choice)

    launch_jar(urls[choice])
    launch_vlc(urls[choice])


if __name__ == "__main__":
    main()